/**
 * appName - http://dvhb.ru
 * @version v0.1.0
 * @author team@dvhb.ru
 */
(function() {


}).call(this);

(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);

setTimeout(function() {
    if($('.select-styler').length){
       $('.select-styler').styler();
    }
    if($('.fancy').length){
        $('.fancy').fancybox();
    }
    if($('.credit-request-popup').length){
        $('.credit-request-popup').trigger('click')
    }
    if($('.slide.active').length){
        $('.slide.active').each(function(){
            $(this).find('.slide-content').slideToggle(200)
        })
    }
    if($('.index-slider').length){
        $('.index-slider').owlCarousel({
            loop: true,
            nav: true,
            items: 1,
            smartSpeed: 500,
            fluidSpeed: 500,
            margin: 0,
            autoplay: true,
            navText: [,]
        });
    }
    if( $('.inp-phone').length){
        $('.inp-phone').mask('+7(999)999-99-99');
    }


    // наведение на статусы

    $('.status-list__el').hover(function(){
        var index = $(this).index() + 1;
        $('.statue-bar-color').stop().animate({
            width: index*300
          }, 300
      );
    })


    $('.js--change-graf').click(function(){
        console.log($(this).index());
        $(this).addClass('active').siblings().removeClass('active');
        $('.profit-scheme').find('ul.profit-point').removeClass('active').eq($(this).attr('data-id')).addClass('active');
        return false;
    });
}, 100);

var timeout_link;

// ввод только цифр в поле количетво
$(document).on('keydown', '.catalog-item-basket-amount', function(e){input_number();});
$(document).on('keydown', '.cart-item-amount-input', function(e){input_number();});
$(document).on('keydown', '.js--input-number', function(e){input_number()});

// ввод количества с клавиатуры
$(document).on('input','.js--input-number', function(){
    if($(this).data("lastval")!= $(this).val()) {

        var value = $(this).prop('value');
        value = value.replace(/\s+/g, '');
        value = Number(value);
        value = value.toString();
        value = number_format(value);
        if(value == "NaN"){
            $(this).prop('value',1)
        }else{
            $(this).prop('value',value);
        }
        $(this).data("lastval", $(this).val());

    };
});
$(document).on('blur','.js--input-number', function(){
    if($(this).val() == '' || $(this).val() == '0'){
        $(this).prop('value',1)
    }
    cart_count($(this).parents('.cart-list__el'))
});


// клик на +
$(document).on('click', '.js--item-shange', function(){
    var current_value = $(this).parents('.catalog-item-basket').find('.catalog-item-basket-amount').prop('value');
    if($(this).hasClass('js--remove-amount') == true && current_value == 0){
        return false
    }else{
        current_value = Number(current_value);
        ($(this).hasClass('js--remove-amount') == true) ? (current_value--) : (current_value++);
        $(this).parents('.catalog-item-basket').find('.catalog-item-basket-amount').prop('value', current_value);
    }
});

// клик на + в корзине
$(document).on('click', '.js--item-change-cart', function(){
    var current_value = $(this).parents('.cart-list__el-amount').find('.cart-item-amount-input').prop('value');
    if($(this).hasClass('js--remove-amount-cart') == true && current_value == 0){
        return false
    }else{
        current_value = Number(current_value);
        ($(this).hasClass('js--remove-amount-cart') == true) ? (current_value--) : (current_value++);
        $(this).parents('.cart-list__el-amount').find('.cart-item-amount-input').prop('value', current_value);
    }
    if(current_value == '0'){
        $(this).parents('.cart-list__el-amount').find('.cart-item-amount-input').prop('value', 1);
    }
    cart_count($(this).parents('.cart-list__el'))
});

// разделение суммы на тройки пробелами
function number_format( str ){
    return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
}

// ввод только цифр в поле
var input_number = function(){
    var allow_meta_keys=[86, 67, 65];
    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9  || event.keyCode == 27 ||
        // Разрешаем: Ctrl+A
        ($.inArray(event.keyCode,allow_meta_keys) > -1 && (event.ctrlKey === true ||  event.metaKey === true)) ||
        // Разрешаем: home, end, влево, вправо
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        // Ничего не делаем
        return;
    }
    else {
        // Обеждаемся, что это цифра, и останавливаем событие keypress
        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
};

$(document).on('click', '.js--add-item', function(){
    var form = $(this).parents('.item-form');
    var method = form.attr('method');
    var action = form.attr('action');

    $.ajax({
        url: action,
        method: method,
        data: form.serialize(),
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);
            form.find('.catalog-item-basket').addClass('item-added');
            form.find('.catalog-item-basket .catalog-item-basket-link a').text("В корзине " + parse_data.amount + " шт.");
            $('.aside-nav__el-basket a span').text(parse_data.total);
        }
    });

    return false;
})

// подгрузка списка
$(document).on('click', '.js--show-more', function(){
    var block = $('.list-to-load');
    var url = block.attr('data-url');
    var method = 'post';

    $.ajax({
        url: url,
        method: method,
        success: function (data) {
            block.append(data);
            if($('.last-el').length){
                block.next('.js--show-more').remove()
            }
        }
    });

})

$(document).on('click', '.slide h2', function(){
    $(this).parents('li').toggleClass('active').find('.slide-content').slideToggle(200)
})


// пересчет итоговой стоимости и доступности кнопки в корзине
var cart_count = function(block){
    var url = $('.cart-list').attr('data-url-chahge');
    var method = $('.cart-list').attr('data-method');
    var id = block.find('.cart-item-id').prop('value');
    var amount = block.find('.cart-item-amount-input').prop('value');

    $.ajax({
        url: url,
        method: method,
        data: {id:id, amount:amount},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);
            block.find('.cart-list__el-total strong').text(parse_data.item_summ);
            $('.cart-total strong').text(parse_data.total_summ);
            if(parse_data.status == 'false'){
                $('.cart-total-block .btn').attr('disabled',true);
                $('.cart-total-block').addClass('disabled');
                $('.cart-disable .not-enought').text(parse_data.enought);
            }
            else{
                $('.cart-total-block .btn').removeAttr('disabled');
                $('.cart-total-block').removeClass('disabled');
            }
        }
    });
}

// удаление позиции в корзине

$(document).on('click', '.js--cart-remove', function(){

    var url = $('.cart-list').attr('data-url-remove');
    var method = $('.cart-list').attr('data-method');
    var block =  $(this).parents('li');
    var id = block.find('.cart-item-id').prop('value');

    $.ajax({
        url: url,
        method: method,
        data: {id:id},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);
            block.remove();
            $('.cart-total strong').text(parse_data.total_summ);
            if(parse_data.status == 'false'){
                $('.cart-total-block .btn').attr('disabled',true);
                $('.cart-total-block').addClass('disabled');
                $('.cart-disable .not-enought').text(parse_data.enought);
            }
            else{
                $('.cart-total-block .btn').removeAttr('disabled');
                $('.cart-total-block').removeClass('disabled');
            }
            if($('.cart-list li').length == '0'){
              $('.cart-full').addClass('hidden-block');
              $('.cart-empty').removeClass('hidden-block');
            }
        }
    });

    return false;
})


 // валидация формы
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    // фокус поля
    $(document).on('focus', '.inp', function(){
        $(this).removeClass('error');
    });

    // отправка формы
    $(document).on('click', '.js-form-submit', function () {
        var form =  $(this).parents('.main-form');
        var errors = false;

        $(form).find('.required').each(function(){
            var val=$(this).prop('value');
            if(val==''){
                $(this).addClass('error');
                errors=true;
            }
            else{
                if($(this).hasClass('inp-mail')){
                    if(validateEmail(val) == false){
                        $(this).addClass('error');
                        errors=true;
                    }
                }
            }
        });

        if(errors == false){
            var button_value = $(form).find('.js-form-submit').prop('value');
            $(form).find('.js-form-submit').prop('value', 'Подождите...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();
            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function(data) {
                    form.find('input').each(function(){
                        $(this).prop('value','')
                    });
                    $(form).find('.js-form-submit').prop('value', button_value);
                    $('.js--form-ok').trigger('click')
                },
                error: function(data) {
                    $(form).find('.js-form-submit').prop('value','Ошибка');
                    setTimeout(function() {
                        $(form).find('.js-form-submit').prop('value', button_value);
                    }, 2000);
                }
            });
        }
        return false;
    });

// прокрутка страницы

$(document).on('click', '.js--page-slide', function(){
$   ("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top - 168 + "px"
    }, {
        duration: 500
    });
    return true;
})
